/*Weather Observation Station 5*/

select top 1 CITY, LEN(CITY) from STATION
where LEN(CITY) = (SELECT min(LEN(CITY))
                   from STATION )
                   ORDER BY CITY;
select top 1 CITY, LEN(CITY) from STATION
where LEN(CITY) = (SELECT max(LEN(CITY))
                   from STATION )
                   ORDER BY CITY;

/*Binary Tree Nodes*/

SELECT 
    CASE 
        WHEN P IS NULL THEN CONCAT(N,' Root')
        WHEN N IN (SELECT P FROM BST) THEN CONCAT(N,' Inner')
        ELSE CONCAT(N,' Leaf')
    END
FROM BST
ORDER BY N

/*New Companies*/

SELECT C.company_code, C. founder, 
        COUNT(DISTINCT L.lead_manager_code),
        COUNT(DISTINCT S.senior_manager_code ),
        COUNT(DISTINCT M.manager_code),
        COUNT(DISTINCT E.employee_code)
FROM Company C, Lead_Manager L, Senior_Manager S,
    Manager M, Employee E
WHERE C.company_code = L.company_code
    AND L.lead_manager_code = S.lead_manager_code
    AND S.senior_manager_code = M.senior_manager_code
    AND M.manager_code = E.manager_code
GROUP BY C.company_code, C. founder
ORDER BY C.company_code;

/*Top Earners*/

SELECT MAX(salary*months), COUNT(salary*months) 
FROM employee 
WHERE salary*months = (SELECT MAX(salary*months) FROM employee)

/*Weather Observation Station 15*/

SELECT CAST(LONG_W AS DECIMAL(10,4))
FROM STATION 
WHERE LAT_N = (SELECT MAX(LAT_N) FROM STATION WHERE LAT_N<137.2345);

/*Weather Observation Station 17*/

SELECT CAST(LONG_W AS DECIMAL(10,4))
FROM STATION 
WHERE LAT_N = (SELECT MIN(LAT_N) FROM STATION WHERE LAT_N>38.7780);

/*Weather Observation Station 20*/

SELECT CAST(LAT_N AS DECIMAL (7,4))
FROM
(SELECT LAT_N, ROW_NUMBER() OVER (ORDER BY LAT_N) AS ROWNU FROM STATION ) AS X
WHERE ROWNU = (SELECT ROUND((COUNT(LAT_N)+1)/2,0) FROM STATION);

/*Population Census*/

SELECT SUM(CI.POPULATION) 
FROM CITY CI, COUNTRY CO
WHERE CI.COUNTRYCODE = CO.CODE AND CO.CONTINENT = 'Asia';

/*African Cities*/

SELECT CI.NAME
FROM CITY CI, COUNTRY CO
WHERE CI.COUNTRYCODE = CO.CODE AND CO.CONTINENT = 'Africa';

/*Average Population of Each Continent*/

SELECT CO.Continent, CAST(AVG(CI.POPULATION) AS DECIMAL(10,0))
FROM City CI JOIN Country CO ON CI.CountryCode=CO.Code
GROUP BY CO.Continent;

/*The Report*/

SELECT 
    CASE
        WHEN G.Grade >= 8
        THEN S.Name
        WHEN G.Grade <8
        THEN NULL
    END, G.Grade, S.Marks 
FROM Students S
    INNER JOIN Grades G
    ON S.Marks BETWEEN G.Min_Mark AND G.Max_Mark 
ORDER BY G.Grade DESC, S.Name ASC, S.Marks ASC;

/*Ollivander's Inventory*/

SELECT sq.id, sq.age, sq.coins_needed, sq.power
FROM (
    SELECT w.id, wp.age, w.coins_needed, w.power,
        ROW_NUMBER() OVER(PARTITION BY w.code, w.power ORDER BY w.power DESC, w.coins_needed ASC) AS rn
        FROM Wands AS w
        INNER JOIN Wands_Property AS wp
        ON wp.code = w.code
        WHERE wp.is_evil = 0
    ) AS sq
WHERE sq.rn = 1
ORDER BY sq.power DESC, sq.age DESC;

/*Challenges*/

SELECT a.hacker_id,a.name,COUNT(b.hacker_id)    
FROM Hackers a, Challenges b
WHERE a.hacker_id = b.hacker_id
GROUP BY a.hacker_id,a.name
HAVING COUNT(b.hacker_id) 
NOT IN (SELECT DISTINCT COUNT(hacker_id) 
        FROM Challenges WHERE hacker_id <> a.hacker_id 
        GROUP BY hacker_id 
        HAVING COUNT(hacker_id) < (SELECT MAX(x.challenge_count) 
                                   FROM (SELECT COUNT(b.challenge_id) as challenge_count 
                                         FROM Challenges b 
                                         GROUP BY b.hacker_id) as x ))
ORDER BY count(b.hacker_id) DESC, a.hacker_id 

/*Contest Leaderboard*/

SELECT h.hacker_id, h.name, SUM(score) 
FROM (
    SELECT hacker_id, challenge_id, MAX(score) AS score 
    FROM SUBMISSIONS
    GROUP BY hacker_id, challenge_id
    ) AS t 
    JOIN Hackers h ON t.hacker_id = h.hacker_id
GROUP BY h.hacker_id, h.name
HAVING SUM(score) > 0
ORDER BY SUM(score) desc, h.hacker_id

/*SQL Project Planning*/

SELECT Start_Date,End_Date FROM (
    SELECT Start_Date,ROW_NUMBER() OVER(Order BY Start_Date) AS ID
    FROM Projects 
    WHERE Start_Date NoT IN (SELECT End_Date FROM Projects)) Starts
JOIN (
    SELECT End_Date,ROW_NUMBER() OVER(Order BY End_Date) AS ID
    FROM Projects 
    WHERE End_Date NoT IN (SELECT Start_Date FROM Projects)) Ends 
ON Starts.ID=Ends.ID
ORDER BY DATEDIFF(DAY,Start_Date,End_Date),Start_Date

/*Symmetric Pairs*/

SELECT f1.X, f1.Y 
FROM Functions f1
    INNER JOIN Functions f2 
    ON f1.X=f2.Y 
    AND f1.Y=f2.X
GROUP BY f1.X, f1.Y
HAVING COUNT(f1.X)>1 or f1.X<f1.Y
ORDER BY f1.X 

/*Interviews*/

SELECT
    contests.contest_id,
    contests.hacker_id,
    contests.name,
    isnull(sum(total_submissions), 0),
    isnull(sum(total_accepted_submissions),0),
    sum(vs.total_views),
    sum(vs.total_unique_views)
from
    contests
    inner join colleges
        on contests.contest_id = colleges.contest_id
    inner join challenges
        on colleges.college_id = challenges.college_id
    left outer join (
              select challenge_id, sum(total_submissions) as total_submissions, sum(total_accepted_submissions) as total_accepted_submissions from submission_stats group by challenge_id
  
    ) submission_stats
        on challenges.challenge_id = submission_stats.challenge_id
    left outer join (
        select challenge_id, sum(Total_views) as total_views, sum(total_unique_views) as total_unique_views from view_stats group by challenge_id
    ) vs
        on challenges.challenge_id = vs.challenge_id
group by
    contests.contest_id,
    contests.hacker_id,
    contests.name
order by
    contests.contest_id

/*15 Days of Learning SQL*/    


SELECT q2.submission_date, q2.Visitors, h.hacker_id, h.name 
FROM
(SELECT d.submission_date, q1.Visitors, d.hacker_id from
(SELECT TOP 2147483647 wks.submission_date, COUNT(hacker_id) as Visitors
FROM (SELECT TOP 2147483647 a.hacker_id, MIN(a.submission_date) AS FinalVisit
      FROM Submissions a 
           INNER JOIN Submissions Firstsubmission_date
           ON a.hacker_id = Firstsubmission_date.hacker_id
      WHERE a.submission_date >= '2016-03-01'
        AND Firstsubmission_date.submission_date = '2016-03-01'
        AND NOT EXISTS (SELECT 1 
                        FROM Submissions b
                        WHERE b.submission_date = dateadd(day, 1, a.submission_date)
                          AND b.hacker_id = a.hacker_id)
      GROUP BY a.hacker_id) fv
     INNER JOIN
     (SELECT DISTINCT submission_date 
      FROM Submissions
      WHERE submission_date >= '2016-03-01') wks
     ON fv.FinalVisit >= wks.submission_date 
GROUP BY wks.submission_date
ORDER BY wks.submission_date) q1
inner join (select c.submission_date, c.hacker_id
from
  (
    select TOP 2147483647 submission_date, hacker_id, count(hacker_id) as cnt, 
    row_number() over(partition by submission_date order by submission_date, count(hacker_id) desc, hacker_id) as rn 
    from Submissions
    group by submission_date, hacker_id
  ) c
where c.rn = 1) d
on q1.submission_date = d.submission_date) q2
inner join Hackers h
on h.hacker_id = q2.hacker_id;